FROM python:3

WORKDIR /usr/src/app

RUN apt update && apt install wkhtmltopdf -y
RUN wget https://gitlab.com/NepsTM/webnovels_dl/-/archive/master/webnovels_dl-master.tar.bz2
RUN tar -xzvf webnovels_dl-master.tar.gz

WORKDIR webnovels_dl-master

RUN pip install --no-cache-dir -r requirements.txt

CMD python ./main_file.py --url="$DL_URL" --name="$DL_NAME" --directory="$DL_PATH"