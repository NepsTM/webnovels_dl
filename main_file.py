import pdfkit
import requests
import sys
from bs4 import BeautifulSoup
import os
import argparse
import shutil


def add_slash(string):
    if not string or string[-1] == '/':
        return string
    return string + '/'

def getNextChapter(soup, website="https://www.royalroad.com"):
    nextChapter = soup.find("link", {"rel" : "next"})
    if nextChapter:
        return website + nextChapter["href"]
    return None

def log10(nb):
    res = 0
    while nb > 0 and nb > 9:
            nb /= 10
            nb = int(nb)
            res += 1
    return res;

def getOes(nb, total_Oes):
    return (total_Oes - log10(nb)) * "0"

def createName(currentNb, book_name):
    return book_name + '-' + getOes(currentNb, 3) + str(currentNb) + ".pdf"


def contentFromChapter(soup):
    meta = '<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>\n' +\
    '<div style="font-size:35px;background: rgba(255, 255, 255, 1)">'
    soupContent = soup.find("div", {"class" : "chapter-inner chapter-content"}).__str__()
    soupContent.replace('<p style="font-style: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-align: center; text-indent: 0; text-transform: none; word-spacing: 0; text-decoration: none; margin: 0 0 12px; font-stretch: normal; font-family: Charter; min-height: 17px">', "")
    soupContent.replace('<p style="font-style: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-indent: 0; text-transform: none; word-spacing: 0; text-decoration: none; margin: 0 0 12px; font-stretch: normal; font-family: Charter"><span style="font-family: &quot;Open Sans&quot;, sans-serif">“', "")
    return  meta + soupContent + "\n</div>"

def getNovel(url, book_name, directory, copy):
    chapNb = 1
    nextChapter = url
    page, soup = None, None
    while url:

        chap_name = createName(chapNb, book_name)
        file_name = directory + chap_name
        print(file_name)

        page = requests.get(url)
        soup = BeautifulSoup(page.content, 'html.parser')

        if not os.path.isfile(file_name):
            print(url)
            content = contentFromChapter(soup)
            pdfkit.from_string(content, file_name)
            if copy:
                shutil.copyfile(file_name,
                                add_slash(copy) + chap_name)
        else:
            print("Already present. Skipping...")
        chapNb += 1
        url = getNextChapter(soup)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='LN downloader\
 from wuxiaworld.co')

    parser.add_argument('-u', '--url', type=str, required=True,
                        help='The url for the LN')
    parser.add_argument('-s', '--name', type=str, required=True,
                        help='The name given to the chapters')
    parser.add_argument('-d', '--directory', type=str, default='./pdfs/',
                       help='The directory in which the download will be done')
    parser.add_argument('-c', '--copy', type=str, default='./new/',
                       help='If set, will copy the pdfs into the given directory')

    args = parser.parse_args()

    print(args.url)
    book_name = args.name
    directory = add_slash(args.directory)
    if not os.path.isdir(directory):
        print(f"Incorrect directory {directory}, creating it..")
        os.mkdir(directory)
    if args.copy and not os.path.isdir(args.copy):
        print(f"Incorrect directory {args.copy}, creating it..")
        os.mkdir(args.copy)

    getNovel(args.url, book_name, directory, args.copy)
